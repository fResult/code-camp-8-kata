/**
 * @param { number } num
 * @return { 'fizz' | 'buzz' | 'fizzbuzz' | number }
 */
function fizzBuzz(num) {
  return !(num % 3) && !(num % 5)
    ? 'fizzbuzz'
    : !(num % 3)
      ? 'fizz'
      : !(num % 5)
        ? 'buzz'
        : num
}

module.exports = fizzBuzz
