/**
 * @template T
 * @param { Array<T> } items
 * @param { T } keyword
 * @return boolean
 */
function findItem_forEach(items, keyword) {
  // Implement by Array.prototype.forEach()
  let hasItem = false
  items.forEach((item) => {
    if (item === keyword)
      hasItem = true
  })
  return hasItem
}

/**
 * @template T
 * @param { Array<T> } items
 * @param { T } keyword
 * @return boolean
 */
function findItem_reduce(items, keyword) {
  // Implement by Array.prototype.reduce()
  return !!items.reduce((prevResult, currentItem) => {
    return prevResult || currentItem === keyword
  }, false)
}

/**
 * @template T
 * @param { Array<T> } items
 * @param { T } keyword
 * @return boolean
 */
function findItem_filter(items, keyword) {
  // Implement by Array.prototype.filter()
  return !!items.filter((item) => item === keyword)[0]
}

/**
 * @template T
 * @param { Array<T> } items
 * @param { T } keyword
 * @return boolean
 */
function findItem_some(items, keyword) {
  // Implement by Array.prototype.some()
  return items.some((item) => item === keyword)
}

module.exports = {
  findItem_reduce,
  findItem_forEach,
  findItem_filter,
  findItem_some
}
