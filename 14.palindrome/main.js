/**
 * @param { number | string } thing
 * @return { boolean }
 */
function isPalindrome(thing) {
  return (
    String(thing)
      .split('')
      .reduceRight((prevChar, currChar) => {
        return prevChar + currChar
      }, '') === String(thing)
  )
}

module.exports = isPalindrome
